package com.twuc.webApp.domain.oneToOne;

import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserAndProfileTest extends JpaTestBase {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserProfileEntityRepository userProfileEntityRepository;

    @Test
    void should_save_user_and_profile() {
        // TODO
        //
        // 请书写一个测试实现如下的功能：
        //
        // Given 一个 UserEntity 和一个 UserProfileEntity。它们并没有持久化。
        // When 持久化 UserEntity
        // Then UserProfileEntity 也一同被持久化了。
        //
        // <--start--
        UserEntity userEntity = new UserEntity(12L);
        UserProfileEntity userProfileEntity = new UserProfileEntity("San", "Zhang");
        userEntity.setUserProfileEntity(userProfileEntity);
        userProfileEntity.setUserEntity(userEntity);
        flushAndClear(em -> {
            userEntityRepository.save(userEntity);
        });
        Optional<UserProfileEntity> userProfile = userProfileEntityRepository.findById(2L);
        assertTrue(userProfile.isPresent());

        // --end->
    }

    @Test
    void should_remove_parent_and_child() {
        // TODO
        //
        // 请书写一个测试：
        //
        // Given 一个持久化了的 UserEntity 和 UserProfileEntity
        // When 删除 UserEntity
        // Then UserProfileEntity 也被删除了
        //
        // <--start-
        UserEntity userEntity = new UserEntity();
        UserProfileEntity userProfileEntity = new UserProfileEntity("San", "Zhang");
        userEntity.setUserProfileEntity(userProfileEntity);
        userProfileEntity.setUserEntity(userEntity);
        flushAndClear(em -> {
            userEntityRepository.save(userEntity);
        });
        flushAndClear(em -> {
            userEntityRepository.deleteById(userEntity.getId());
        });
        Optional<UserEntity> user = userEntityRepository.findById(userEntity.getId());
        Optional<UserProfileEntity> profile = userProfileEntityRepository.findById(userProfileEntity.getId());
        assertFalse(user.isPresent());
        assertFalse(profile.isPresent());
        // --end->
    }
}