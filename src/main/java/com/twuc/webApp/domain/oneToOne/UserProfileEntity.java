package com.twuc.webApp.domain.oneToOne;

import javax.persistence.*;

// TODO
//
// 请创建 UserEntity 和 UserProfileEntity 之间的 one-to-one 关系。并且确保 UserProfileEntity
// 的数据表结构如下：
//
// user_profile_entity
// +─────────────────+──────────────+──────────────────────────────+
// | column          | type         | additional                   |
// +─────────────────+──────────────+──────────────────────────────+
// | id              | bigint       | primary key, auto_increment  |
// | first_name      | varchar(32)  | not null                     |
// | last_name       | varchar(32)  | not null                     |
// | user_entity_id  | bigint       | not null                     |
// +─────────────────+──────────────+──────────────────────────────+
//
// <--start-
@Entity
public class UserProfileEntity {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false,length = 32)
    private String firstName;
    @Column(nullable = false,length = 32)
    private String lastName;
    @OneToOne(orphanRemoval = true)
    private UserEntity userEntity;

    public UserProfileEntity() {
    }

    public UserProfileEntity(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }
}
